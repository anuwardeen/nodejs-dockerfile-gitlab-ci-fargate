FROM node:12.16-slim

COPY package.json .
COPY package-lock.json .

RUN npm ci

COPY app.js .

EXPOSE 3000

CMD ["node", "app.js"]
