# Node.js and Dockerfile blend for GitLab CI on AWS Fargate

Sample Node.js repository demonstrating how to run GitLab CI pipelines with the
[AWS Fargate Custom Executor driver](https://gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/fargate)
for [GitLab Runner](https://docs.gitlab.com/runner).

[![pipeline
status](https://gitlab.com/aws-fargate-driver-demo/nodejs-dockerfile-gitlab-ci-fargate/badges/master/pipeline.svg)](https://gitlab.com/aws-fargate-driver-demo/nodejs-dockerfile-gitlab-ci-fargate/-/commits/master)

The CI/CD pipeline for this repository is a bit more complex than that from
[Node.js sample for GitLab CI on AWS Fargate](https://gitlab.com/aws-fargate-driver-demo/gitlab-ci-fargate-sample)
in the sense this one builds and pushes a Docker image to a given container
registry every time a new Git Tag is created. It leverages
[GoogleContainerTools/kaniko](https://github.com/GoogleContainerTools/kaniko) to
handle the Docker image, which means the pipeline runs partially on AWS Fargate
and partially in a Docker-enabled Runner.
